/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2;

import inventorysystemtjacqu2.controller.MainScreenController;
import inventorysystemtjacqu2.controller.PartAddController;
import inventorysystemtjacqu2.controller.PartModController;
import inventorysystemtjacqu2.controller.ProductAddController;
import inventorysystemtjacqu2.controller.ProductModController;
import inventorysystemtjacqu2.model.InHouse;
import inventorysystemtjacqu2.model.Inventory;
import inventorysystemtjacqu2.model.OutSourced;
import inventorysystemtjacqu2.model.Product;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author tomjacques
 */
public class InventoryMain extends Application {
    //another comment
    //2nd test comment
    //comment 2
    private Stage invStage;
    private BorderPane rootLayout;
    Image image = new Image("gears1.jpg");
    
    public InventoryMain() {
        OutSourced testPart1 = new OutSourced("OS bearing2", 4.00, 3, 1, 5, "A inc.");
        OutSourced testPart2 = new OutSourced("OS gear1", 3.00, 10, 1, 10, "B inc.");
        OutSourced testPart3 = new OutSourced("OS gear2", 2.00, 10, 1, 10, "C inc.");
        OutSourced testPart4 = new OutSourced("OS cog1", 1.00, 14, 1, 20, "D inc.");
        InHouse testPart5 = new InHouse("IH cog2", 5.00, 16, 1, 20, 20);
        InHouse testPart6 = new InHouse("IH bearing1", 6.00, 16, 1, 20, 30);
        InHouse testPart7 = new InHouse("IH bearing3", 3.00, 5, 1, 10, 40);
        Product testProduct1 = new Product("Engine 1", 5, 11.00, 1, 10, testPart1);
        Product testProduct2 = new Product("Engine 2", 4, 10.00, 1, 10, testPart2);
        testProduct1.addAssociatedPart(testPart5);
        testProduct2.addAssociatedPart(testPart6);
        //Product testProduct2 = new Product("Engine 2(no parts)", 3, 0.00, 2, 8);
        Inventory.InvPartsData.add(testPart1);
        Inventory.InvPartsData.add(testPart2);
        Inventory.InvPartsData.add(testPart3);
        Inventory.InvPartsData.add(testPart4);
        Inventory.InvPartsData.add(testPart5);
        Inventory.InvPartsData.add(testPart6);
        Inventory.InvPartsData.add(testPart7);
        Inventory.InvProductData.add(testProduct1);
        Inventory.InvProductData.add(testProduct2);
        //Inventory.InvProductData.add(testProduct2);
    }
    
    @Override
    public void start(Stage invStage) throws Exception {
        this.invStage = invStage;
        this.invStage.setTitle("InventoryControl");
        
        initRootLayout();
        showMainLayout();
    }
    /**
     * initializes root layout
     */
    public void initRootLayout() {
    	try {
            //Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(InventoryMain.class.getResource("controller/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            invStage.setScene(scene);
            invStage.show();
            invStage.setResizable(false);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * loads main layout
     */
    public void showMainLayout() {
    	try {
            // Load Inventory mainscreen.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(InventoryMain.class.getResource("controller/MainScreen.fxml"));
            AnchorPane mainScreen = (AnchorPane) loader.load();
            
            /*ImageView image1 = new ImageView();
            image1.setImage(image);
            mainScreen.getChildren().add(image1);
            image1.fitWidthProperty().bind(mainScreen.widthProperty());
            image1.fitHeightProperty().bind(mainScreen.heightProperty());
            */
            
            // Set mainscreen into the center of root layout.
            rootLayout.setCenter(mainScreen);
            // Give the controller access to the main app.
            MainScreenController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * loads part add screen
     */
    public void showPartAdd() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(InventoryMain.class.getResource("controller/PartAdd.fxml"));
            AnchorPane partAdd = (AnchorPane) loader.load();
            
            // Create the secondary Stage.
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Add Part");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(invStage);
            Scene scene = new Scene(partAdd);
            secondaryStage.setScene(scene);
            secondaryStage.setResizable(false);
            
            PartAddController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
            
        }catch (IOException e) {
            e.printStackTrace();
            }
    }
    
    /**
     * loads product add screen
     */
    public void showProdAdd() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(InventoryMain.class.getResource("controller/ProductAdd.fxml"));
            AnchorPane prodAdd = (AnchorPane) loader.load();

            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Add Product");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(invStage);
            Scene scene = new Scene(prodAdd);
            secondaryStage.setScene(scene);
            secondaryStage.setResizable(false);

            ProductAddController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * loads part modification screen
     */
    public void showPartMod() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(InventoryMain.class.getResource("controller/PartMod.fxml"));
            AnchorPane partMod = (AnchorPane) loader.load();
            // Create the secondary Stage.
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Modify Part");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(invStage);
            Scene scene = new Scene(partMod);
            secondaryStage.setScene(scene);
            secondaryStage.setResizable(false);

            PartModController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
            }catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * loads product modification screen
     */
    public void showProdMod() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(InventoryMain.class.getResource("controller/ProductMod.fxml"));
            AnchorPane prodMod = (AnchorPane) loader.load();
            // Create the secondary Stage.
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Modify Product");
            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(invStage);
            Scene scene = new Scene(prodMod);
            secondaryStage.setScene(scene);
            secondaryStage.setResizable(false);

            ProductModController controller = loader.getController();
            controller.setSecondaryStage(secondaryStage);
            controller.setMainApp(this);
            secondaryStage.showAndWait();
            }catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    	/**
         * Returns the main stage.
         *
         * @return
         */
        public Stage getInvStage() {
            return invStage;
        }
    	
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.controller;

import inventorysystemtjacqu2.InventoryMain;
import inventorysystemtjacqu2.model.InHouse;
import inventorysystemtjacqu2.model.Inventory;
import inventorysystemtjacqu2.model.OutSourced;
import inventorysystemtjacqu2.model.Part;
import inventorysystemtjacqu2.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;


/**
 * FXML Controller class
 *
 * @author tomjacques
 */
//testing git
//test 2
//test3
//test6
public class MainScreenController {
     
    
    @FXML
    private Button btnPartAdd;

    @FXML
    private Button btnPartMod;

    @FXML
    private Button btnPartDelete;

    @FXML
    private Button btnProdAdd;

    @FXML
    private Button btnProdMod;

    @FXML
    private Button btnProdDelete;

    @FXML
    private Button btnExit;

    @FXML
    private Button btnCancelPartSearch;

    @FXML
    private Button btnCancelProdSearch;

    @FXML
    private TextField searchPartField;// = new TextField();

    @FXML
    private TextField searchProdField;
    
    @FXML
    private TableView<Part> PartTable;
    
    @FXML
    private TableColumn<Part, String> PartNameColumn;
    
    @FXML
    private TableColumn<Part, Integer> PartIDColumn;
    
    @FXML
    private TableColumn<Part, Integer> PartStockColumn;
    
    @FXML
    private TableColumn<Part, Double> PartPriceColumn;
    
    @FXML
    private TableView<Product> ProductTable;
    
    @FXML
    private TableColumn<Product, String> ProductNameColumn;
    
    @FXML
    private TableColumn<Product, Integer> ProductIDColumn;
    
    @FXML
    private TableColumn<Product, Double> ProductPriceColumn;
    
    @FXML
    private TableColumn<Product, Integer> ProductStockColumn;
    
    private InventoryMain mainApp;
    static int popPartID;
    static String popPartName;
    static int popPartInv;
    static double popPartPrice;
    static int popPartMin;
    static int popPartMax;
    static int popMachineID;
    static String popCompanyName;
    static int popProdID;
    static String popProdName;
    static int popProdInv;
    static double popProdPrice;
    static int popProdMin;
    static int popProdMax;
    static boolean companyMachineBool;
    static ObservableList<Part> PopParts  = FXCollections.observableArrayList();
    
    public MainScreenController() {
    	
    }
    
    @FXML
    private void initialize() {
        // Initialize the Part table.
        PartIDColumn.setCellValueFactory(cellData -> cellData.getValue().partIDProperty().asObject());
        PartNameColumn.setCellValueFactory(cellData -> cellData.getValue().partNameProperty());
        PartPriceColumn.setCellValueFactory(cellData -> cellData.getValue().partPriceProperty().asObject());
        PartStockColumn.setCellValueFactory(cellData -> cellData.getValue().partInStockProperty().asObject());
        
        // Initialize the Product table.
        ProductNameColumn.setCellValueFactory(cellData -> cellData.getValue().prodNameProperty());
        ProductIDColumn.setCellValueFactory(cellData -> cellData.getValue().prodIDProperty().asObject());
        ProductStockColumn.setCellValueFactory(cellData -> cellData.getValue().prodInStockProperty().asObject());
        ProductPriceColumn.setCellValueFactory(cellData -> cellData.getValue().prodPriceProperty().asObject());
        searchPartField.textProperty().addListener((observable, oldValue, newValue) -> handlePartSearchAction());
        searchProdField.textProperty().addListener((observable, oldValue, newValue) -> handleProdSearchAction());
        
    }
    //@FXML
    //public TextField getPartSearchProperty() {
    //    return searchPartField;
    //}    
    
    @FXML
    private void handleBtnExit() {
    	 	System.exit(0);
    }
    
    @FXML
    private void handleBtnPartAdd() {
        mainApp.showPartAdd();
    }
    
    /**
     * handles part deletion from main screen parts table
     */
    @FXML
    void handleBtnPartDelete() {
        Part chosenPart	= PartTable.getSelectionModel().getSelectedItem();
        
        if(chosenPart != null) {
            if (isPartInProduct(chosenPart.getPartName())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Alert");
                alert.setHeaderText("Part was located in a product");
                alert.setContentText("A part associated with a product cannot be deleted");
                alert.showAndWait();
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Part Delete!");
                alert.setHeaderText("Part Will Be Deleted From Database!");
                alert.setContentText("Are you sure you want to delete this part?");
                alert.showAndWait()
                    .filter(response -> response == ButtonType.OK)
                    .ifPresent(response -> Inventory.getInvPartsData().remove(chosenPart));
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Part Selected for Delete");
            alert.setContentText("A part in the left table must be selected for deletion");
            alert.showAndWait();
        }
    }

    /**
     * loads part to be modified into part modification screen
     */
    @FXML
    private void handleBtnPartMod() {
        Part modPart = PartTable.getSelectionModel().getSelectedItem();
            if(modPart != null) {
                if (modPart.isGetCompany()) {
                    popPartID = modPart.getPartID();
                    popPartName = modPart.getPartName();
                    popPartInv = modPart.getPartInStock();
                    popPartPrice = modPart.getPartPrice();
                    popPartMin = modPart.getPartMin();
                    popPartMax = modPart.getPartMax();
                    popCompanyName = ((OutSourced)modPart).getCompanyName();
                    companyMachineBool = true;
                }else{
                    popPartID = modPart.getPartID();
                    popPartName = modPart.getPartName();
                    popPartInv = modPart.getPartInStock();
                    popPartPrice = modPart.getPartPrice();
                    popPartMin = modPart.getPartMin();
                    popPartMax = modPart.getPartMax();
                    popMachineID = ((InHouse)modPart).getMachineID();
                    companyMachineBool = false;
                }                
            mainApp.showPartMod();
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Part is Selected");
            alert.setContentText("A part must be selected from the left table");
            alert.showAndWait();
        }
    }

    /**
     * handles part search when typing in search text field
     */
    private void handlePartSearchAction() {
        String searchText = searchPartField.getText();
        FilteredList<Part> searchPartResults = searchParts(searchText);
        SortedList<Part> sortedData = new SortedList<>(searchPartResults);
        sortedData.comparatorProperty().bind(PartTable.comparatorProperty());
        PartTable.setItems(sortedData);
        if (searchPartResults.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Part Not Found");
            alert.setContentText("No part was found. Re-try search");
            alert.showAndWait();
        }
    }
    
    /**
     * searches part by part ID or name
     * @param s search string or int
     * @return part match
     */
    private FilteredList<Part> searchParts(String s){
        try {
           Integer.parseInt(s);
           return Inventory.getInvPartsData().filtered(p -> Integer.toString(p.getPartID()).contains(s));
        } catch(NumberFormatException e) {
           return Inventory.getInvPartsData().filtered(p -> p.getPartName().toLowerCase().contains(s.toLowerCase())); 
        }
    }
    
    /**
     * clears search text field 
     */
    @FXML
    private void handleBtnCancelPartSearch() {
        searchPartField.setText("");
    }
    /*@FXML
    private void handlePartSearchAction() {    
        boolean partFound = false;
        String partSearch = searchPartField.getText();
        try {
            int IDNumber = Integer.parseInt(partSearch);
            for (Part p : Inventory.InvPartsData) {
                if (p.getPartID() == IDNumber) {
                    partFound = true;
                    //send to table here
                    System.out.println(p.getPartID() + " " + p.getPartName() + " " + p.getPartPrice());
                }
            }
            if (partFound == false) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Part Not Found");
                    alert.setContentText("Part was not found. Re-try search");
                    alert.showAndWait();
            }
        }catch(NumberFormatException e){
            for (Part p : Inventory.InvPartsData) {
                if (partSearch.equals(p.getPartName())) {
                    partFound = true;
                    //send to table here
                    System.out.println(p.getPartID() + " " + p.getPartName() + " " + p.getPartPrice());
                }
            }
            if (partFound == false) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Part Not Found");
                    alert.setContentText("Part was not found. Re-try search");
                    alert.showAndWait();
            }
        }
    }*/
    
    /**
     * loads product add screen
     */
    @FXML
    private void handleBtnProdAdd() {
        mainApp.showProdAdd();
    }
    
    /**
     * deletes a product with no parts
     */
    @FXML
    void handleBtnProdDelete() {
        Product chosenProd = ProductTable.getSelectionModel().getSelectedItem();
        
        if(chosenProd != null) {
            if (chosenProd.getAssociatedParts().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Product Delete!");
                alert.setHeaderText("Product Will Be Deleted From Database!");
                alert.setContentText("Are you sure you want to delete this product?");
                alert.showAndWait()
                    .filter(response -> response == ButtonType.OK)
                    .ifPresent(response -> Inventory.getInvProductData().remove(chosenProd));
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Delete Not Allowed");
                alert.setHeaderText("Product Cannot Be Deleted");
                alert.setContentText("A product with a part cannot be deleted");
                alert.showAndWait(); 
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Product Selected for Delete");
            alert.setContentText("A product in the right table must be selected for deletion");
            alert.showAndWait();
        }
    }

    /**
     * loads selected product into product modification screen
     */
    @FXML
    void handleBtnProdMod() {
        Product modProd = ProductTable.getSelectionModel().getSelectedItem();
        if(modProd != null) {
            popProdID = modProd.getProdID();
            popProdName = modProd.getProdName();
            popProdInv = modProd.getProdInStock();
            popProdPrice = modProd.getProdPrice();
            popProdMin = modProd.getProdMin();
            popProdMax = modProd.getProdMax();
            PopParts = modProd.getAssociatedParts();
            mainApp.showProdMod();
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Product is Selected");
            alert.setContentText("A product must be selected from the right table");
            alert.showAndWait();
        }
    }

    /**
     * handles product search when typing in search text field
     */
    @FXML
    void handleProdSearchAction() {
        String searchText = searchProdField.getText();
        FilteredList<Product> searchProductResults = searchProducts(searchText);
        SortedList<Product> sortedData = new SortedList<>(searchProductResults);
        sortedData.comparatorProperty().bind(ProductTable.comparatorProperty());
        ProductTable.setItems(sortedData);
        if (searchProductResults.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
           alert.setTitle("Alert");
           alert.setHeaderText("Product Not Found");
           alert.setContentText("No product was found. Re-try search");
           alert.showAndWait();
        }
    }
    
    /**
     * searches product by part ID or name
     * @param s search string or int
     * @return product match
     */
    public FilteredList<Product> searchProducts(String s){
        try {
           Integer.parseInt(s);
           return Inventory.getInvProductData().filtered(p -> Integer.toString(p.getProdID()).contains(s));
        } catch(NumberFormatException e) {
           return Inventory.getInvProductData().filtered(p -> p.getProdName().toLowerCase().contains(s.toLowerCase())); 
        }
    }
    
    /**
     * clears search text field
     */
    @FXML
    private void handleBtnCancelProdSearch() {
       searchProdField.setText(""); 
    }
    /*@FXML
    void handleProdSearchAction(ActionEvent event) {
        boolean partFound = false;
        String prodSearch = searchProdField.getText();
        try {
            int IDNumber = Integer.parseInt(prodSearch);
            for (Product p : Inventory.InvProductData) {
                if (p.getProdID() == IDNumber) {
                    partFound = true;
                    //send to table here
                    System.out.println(p.getProdID() + " " + p.getProdName() + " " + p.getProdPrice());
                }
            }
            if (partFound == false) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Product Not Found");
                    alert.setContentText("Product was not found. Re-try search");
                    alert.showAndWait();
            }
        }catch(NumberFormatException e){
            for (Product p : Inventory.InvProductData) {
                if (prodSearch.equals(p.getProdName())) {
                    partFound = true;
                    //send to table here
                    System.out.println(p.getProdID() + " " + p.getProdName() + " " + p.getProdPrice());
                }
            }
            if (partFound == false) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Product Not Found");
                    alert.setContentText("Product was not found. Re-try search");
                    alert.showAndWait();
            }
        }
    }*/
    
    /**
     * checks if part set for deletion is located in a product
     * @param partName part name
     * @return true if part name match
     */
    private boolean isPartInProduct(String partName) {
        boolean match = false;
            for (Product p : Inventory.InvProductData) {
                for (Part pt : p.getAssociatedParts()) {                    
                    if (pt.getPartName().equals(partName)) {
                       match = true; 
                    }
                }
            }
          return match;  
    }
    
    /**
     * sets parts and products into tables
     * @param mainApp 
     */
    public void setMainApp(InventoryMain mainApp) {
        this.mainApp = mainApp;
        PartTable.setItems(Inventory.getInvPartsData());
        ProductTable.setItems(Inventory.getInvProductData());
    }
    /**
     * Initializes the controller class
     * @param url
     * @param rb
     */
    //@Override
   // public void initialize(URL url, ResourceBundle rb) {
        // TODO
    //}    
    
}

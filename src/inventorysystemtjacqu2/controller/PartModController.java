/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.controller;


import inventorysystemtjacqu2.InventoryMain;
import static inventorysystemtjacqu2.controller.MainScreenController.companyMachineBool;
import inventorysystemtjacqu2.model.InHouse;
import inventorysystemtjacqu2.model.Inventory;
import inventorysystemtjacqu2.model.OutSourced;
import inventorysystemtjacqu2.model.Part;
import inventorysystemtjacqu2.model.Product;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class PartModController {

    @FXML
    private AnchorPane labelPriceCost;

    @FXML
    private RadioButton btnRadioOutSrcd;

    @FXML
    private RadioButton btnRadioInHse;

    @FXML
    private Label labelID;

    @FXML
    private Label labelName;

    @FXML
    private Label labelInv;

    @FXML
    private Label labelMin;

    @FXML
    private Label CompanyMachineLabel;

    @FXML
    private Label labelModPart;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private Label labelMax;
    
    @FXML
    private TextField partIDField;
    
    @FXML
    private TextField partNameField;
    
    @FXML
    private TextField partPriceField;
    
    @FXML
    private TextField partInStockField;
    
    @FXML 
    private TextField partMinField;
    
    @FXML
    private TextField partMaxField;
    
    @FXML
    private TextField companyMachine;
    
    private InventoryMain mainApp;
    private Stage secondaryStage;
    private Part part;
    //private static boolean overwriteBool = false;
    public static boolean updateProduct = false;
    public static String partNameSearch = "";
    @FXML
    private void initialize() {
        partIDField.setText(Integer.toString(MainScreenController.popPartID));
        partNameField.setText(MainScreenController.popPartName);
        partInStockField.setText(Integer.toString(MainScreenController.popPartInv));
        partPriceField.setText(Double.toString(MainScreenController.popPartPrice));
        partMinField.setText(Integer.toString(MainScreenController.popPartMin));
        partMaxField.setText(Integer.toString(MainScreenController.popPartMax));
        partNameField.textProperty().addListener((observable, oldValue, newValue) -> handleNameInput(newValue));
        if (companyMachineBool) {
            companyMachine.setText(MainScreenController.popCompanyName);
            btnRadioOutSrcd.setSelected(true);
            setCompanyMachineLabel("Company Name");
        }
        else {
            companyMachine.setText(Integer.toString(MainScreenController.popMachineID));
            btnRadioInHse.setSelected(true);
            setCompanyMachineLabel("Machine ID");
        }
    }
    
    /**
     * shows alert if name input starts with a number 
     * @param s name string
     */
    private void handleNameInput(String s) {
        try {
            Integer.parseInt(partNameField.getText());

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Part name cannot begin with a number");
            alert.setContentText("Begin part name with a letter or symbol");
            alert.showAndWait();
            partNameField.setText("");
        } catch(NumberFormatException e) { 
            //do nothing, string is ok
        }
    }
    
    /**
     * checks if added part name matches an existing part name
     * @return true if matched name found
     */
    private boolean handlePartNameMatch() {
        String searchText = partNameField.getText();
        FilteredList<Part> searchPartResults = searchParts(searchText);
        //SortedList<Part> sortedData = new SortedList<>(searchPartResults);
        return !searchPartResults.isEmpty();
    }
    
    /**
     * searches part list
     * @param s name string
     * @return part name
     */
    private FilteredList<Part> searchParts(String s) {
        try {
           Integer.parseInt(s);
           return Inventory.getInvPartsData().filtered(p -> Integer.toString(p.getPartID()).equals(s));
        } catch(NumberFormatException e) {
           return Inventory.getInvPartsData().filtered(p -> p.getPartName().toLowerCase().equals(s.toLowerCase())); 
        }
    }
    
    /**
     * handles quit of part addition
     */
    @FXML
    private void handleBtnCancel() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quit");
        alert.setHeaderText("Quit Part Mod Action?");
        alert.setContentText("Would you like to quit modification of this part?");
        alert.showAndWait()
            .filter(response -> response == ButtonType.OK)
            .ifPresent(response -> secondaryStage.close());
    }

    /**
     * sets label to Machine ID when selected
     */
    @FXML
    private void handleBtnRadioInHse() {
        if (btnRadioInHse.isSelected()) {
            setCompanyMachineLabel("Machine ID");
        }
    }

    /**
     * sets label to Company Name when selected
     */
    @FXML
    private void handleBtnRadioOutSrcd() {
        if (btnRadioOutSrcd.isSelected()) {
            setCompanyMachineLabel("Company Name");
        }
    }

    /**
     * saves added part
     */
    @FXML
    void handleBtnSave() {
        if (partNameField.getText().isEmpty() 
            || partPriceField.getText().isEmpty() || partInStockField.getText().isEmpty()
            || partMinField.getText().isEmpty() || partMaxField.getText().isEmpty()
            || companyMachine.getText().isEmpty()) 
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Fields");
            alert.setHeaderText("All Fields Not Populated");
            alert.setContentText("All fields must be filled out");
            alert.showAndWait();
        }
        else if (!(btnRadioInHse.isSelected() || btnRadioOutSrcd.isSelected())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No source selection made");
            alert.setContentText("Must choose In-House or OutSourced button");
            alert.showAndWait();
        }   
        else {
            boolean checkFormat = true;            
            try {
                Integer.parseInt(partInStockField.getText());
            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Inv");
            }
            try {
                Double.parseDouble(partPriceField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A number must be entered for Price/Cost");
            }
            try {
                Integer.parseInt(partMinField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Min");
            }
            try {
                Integer.parseInt(partMaxField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Max");
            }
            if (Integer.parseInt(partMinField.getText()) > Integer.parseInt(partMaxField.getText())) {
                checkFormat = false;
                alertFunction("Min cannot be greater than Max");
            }
            if (Integer.parseInt(partInStockField.getText()) < Integer.parseInt(partMinField.getText()) 
                || Integer.parseInt(partInStockField.getText()) > Integer.parseInt(partMaxField.getText())) {
                checkFormat = false;
                alertFunction("Inv cannot be less than min or greater than max");
            }
            if (checkFormat == true) {
                    if (handlePartNameMatch()) {
                        if (!partNameField.getText().equals(MainScreenController.popPartName)) {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Alert");
                            alert.setHeaderText("Duplicate name in parts list");
                            alert.setContentText("Change modified part name");
                            alert.showAndWait();
                            checkFormat = false;
                        }
                    }
            }
            if (btnRadioOutSrcd.isSelected()){    
                if (checkFormat == true) { 
                    part = new OutSourced(Integer.parseInt(partIDField.getText()), partNameField.getText(), Double.parseDouble(partPriceField.getText()), 
                    Integer.parseInt(partInStockField.getText()), Integer.parseInt(partMinField.getText()), 
                    Integer.parseInt(partMaxField.getText()), companyMachine.getText());
                    Inventory.getInvPartsData().set(Integer.parseInt(partIDField.getText()) - 1, part);
                    //partNameSearch = partNameField.getText();
                    partNameSearch = MainScreenController.popPartName;
                    searchAndReplacePrice(part, partNameSearch, partNameField.getText());
                    mainApp.showMainLayout();
                    setMainApp(mainApp);
                }
            }
            
            if (btnRadioInHse.isSelected()) {
                try {
                    Integer.parseInt(companyMachine.getText());
                    
                }
                catch( NumberFormatException e ) {
                    checkFormat = false;
                    alertFunction("A whole number must be entered for Machine ID");
                }
                if (checkFormat == true) {
                    part = new InHouse(Integer.parseInt(partIDField.getText()), partNameField.getText(), Double.parseDouble(partPriceField.getText()), 
                    Integer.parseInt(partInStockField.getText()), Integer.parseInt(partMinField.getText()), 
                    Integer.parseInt(partMaxField.getText()), Integer.parseInt(companyMachine.getText()));
                    Inventory.getInvPartsData().set(Integer.parseInt(partIDField.getText()) - 1, part);
                    partNameSearch = MainScreenController.popPartName;
                    searchAndReplacePrice(part, partNameSearch, partNameField.getText());
                    mainApp.showMainLayout();
                    setMainApp(mainApp);
                }                    
            }
        }
    }
    
    /**
     * handles alert messages
     * @param message message string
     */
    private void alertFunction(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alert");
        alert.setHeaderText("Number Format Incorrect");
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    /**
     * searches for existing part in products and replaces it with new part
     * also updates product pricing by keeping existing profit margin
     * @param newPart the part that replaces the existing part
     * @param partNameSearch the part name to match
     * @param newPartName the new part name
     */
    public static void searchAndReplacePrice(Part newPart, String partNameSearch, String newPartName) {
        double partPricesTotal = 0;
        double productNewPrice = 0;
        double profitMargin = 0;
            for (Product p : Inventory.InvProductData) {
                for (Part pt : p.getAssociatedParts()) {                    
                    if (pt.getPartName().equals(partNameSearch)) {
                        //start add part prices, get profit margin
                        for (Part pt1 : p.getAssociatedParts()) {
                            partPricesTotal += pt1.getPartPrice();
                        }
                        profitMargin = p.getProdPrice()/partPricesTotal;
                        partPricesTotal = 0;
                        //end add part prices, get profit margin
                        pt.setPartPrice(newPart.getPartPrice());
                        pt.setPartName(newPartName);
                        //start add part prices again with new part price, recalc product price
                        for (Part pt2 : p.getAssociatedParts()) {
                            partPricesTotal += pt2.getPartPrice();
                        } 
                        productNewPrice = Math.round(profitMargin * partPricesTotal * 100.0)/100.0;
                        p.setProdPrice(productNewPrice);
                        //end add part prices again with new part price, recalc product price
                    }
                }
            }
    }
    
    /**
     * sets machine or company label
     * @param compMachine machine or company label
     */
    private void setCompanyMachineLabel(String compMachine){
        CompanyMachineLabel.setText(compMachine);
    }
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    
    public void setMainApp(InventoryMain mainApp) {
        this.mainApp = mainApp;
        secondaryStage.close();
    }
    
    /**
     * Initializes the controller class.
     */
    //@Override
    //public void initialize(URL url, ResourceBundle rb) {
        // TODO
    //}    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.controller;



import inventorysystemtjacqu2.InventoryMain;
import inventorysystemtjacqu2.model.InHouse;
import inventorysystemtjacqu2.model.Inventory;
import inventorysystemtjacqu2.model.OutSourced;
import inventorysystemtjacqu2.model.Part;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
//public class PartAddController {
public class PartAddController implements Initializable {    
    @FXML
    private RadioButton btnRadioOutSrcd;

    @FXML
    private RadioButton btnRadioInHse;

    @FXML
    private Label labelID;

    @FXML
    private Label labelName;

    @FXML
    private Label labelInv;

    @FXML
    private Label labelPriceCost;

    @FXML
    private Label labelMax;

    @FXML
    private Label CompanyMachineLabel;
    
    @FXML
    private Label chooseLabel;

    @FXML
    private Label labelAddPart;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private Label labelMin;
    
    @FXML
    private TextField partIDField;
    
    @FXML
    private TextField partNameField;
    
    @FXML
    private TextField partPriceField;
    
    @FXML
    private TextField partInStockField;
    
    @FXML 
    private TextField partMinField;
    
    @FXML
    private TextField partMaxField;
    
    @FXML
    private TextField companyMachine;
    
    
    private InventoryMain mainApp;
    private Stage secondaryStage;
    private Part part;

    /**
     * listener for name text field to detect string or int
     */
    /*@FXML
    private void initialize() {
       partNameField.textProperty().addListener((observable, oldValue, newValue) -> handleNameInput(newValue)); 
    }*/
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        partNameField.textProperty().addListener((observable, oldValue, newValue) -> handleNameInput(newValue));
        
    }   
    /**
     * shows alert if name input starts with a number 
     * @param s name string
     */
    @FXML
    private void handleNameInput(String s) {
        try {
            Integer.parseInt(partNameField.getText());

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Part name cannot begin with a number");
            alert.setContentText("Begin part name with a letter or symbol");
            alert.showAndWait();
            partNameField.setText("");
        } catch(NumberFormatException e) { 
            //do nothing, string is ok
        }
    }
    
    /**
     * checks if added part name matches an existing part name
     * @return true if matched name found
     */
    private boolean handlePartNameMatch() {
        String searchText = partNameField.getText();
        FilteredList<Part> searchPartResults = searchParts(searchText);
        return !searchPartResults.isEmpty();
    }
    
    /**
     * searches part list
     * @param s name string
     * @return part name
     */
    private FilteredList<Part> searchParts(String s){
        try {
           Integer.parseInt(s);
           return Inventory.getInvPartsData().filtered(p -> Integer.toString(p.getPartID()).equals(s));
        } catch(NumberFormatException e) {
           return Inventory.getInvPartsData().filtered(p -> p.getPartName().toLowerCase().equals(s.toLowerCase())); 
        }
    }
    
    /**
     * handles quit of part addition
     */
    @FXML
    private void handleBtnCancel() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quit");
        alert.setHeaderText("Quit Part Add Action?");
        alert.setContentText("Would you like to quit addition of this part?");
        alert.showAndWait()
            .filter(response -> response == ButtonType.OK)
            .ifPresent(response -> secondaryStage.close());
            //secondaryStage.close();
    }
    
    @FXML
    public void setSecondaryStage(Stage secondaryStage) {
    		this.secondaryStage = secondaryStage;
    }
    
    /**
     * sets label to Machine ID when selected
     */
    @FXML
    private void handleBtnRadioInHse() {
        if (btnRadioInHse.isSelected()) {
            setCompanyMachineLabel("Machine ID");
            setChooseLabel("");
        }	
    }

    /**
     * sets label to Company Name when selected
     */
    @FXML
    private void handleBtnRadioOutSrcd() {
        if (btnRadioOutSrcd.isSelected()) {
            setCompanyMachineLabel("Company Name");
            setChooseLabel("");
        }
    }

    /**
     * saves added part
     */
    @FXML
    private void handleBtnSave() {
        if (partNameField.getText().isEmpty() 
            || partPriceField.getText().isEmpty() || partInStockField.getText().isEmpty()
            || partMinField.getText().isEmpty() || partMaxField.getText().isEmpty()
            || companyMachine.getText().isEmpty()) 
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Fields");
            alert.setHeaderText("All Fields Not Populated");
            alert.setContentText("All fields must be filled out");
            alert.showAndWait();
        }
        else if (handlePartNameMatch()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Duplicate Part Name");
            alert.setContentText("Rename Part");
            alert.showAndWait();
        }
        else if (!(btnRadioInHse.isSelected() || btnRadioOutSrcd.isSelected())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No source selection made");
            alert.setContentText("Must choose In-House or OutSourced button");
            alert.showAndWait();
        }   
        else {
            boolean checkFormat = true;
            try {
                Integer.parseInt(partInStockField.getText());
            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Inv");
            }
            try {
                Double.parseDouble(partPriceField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A number must be entered for Price/Cost");
            }
            try {
                Integer.parseInt(partMinField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Min");
            }
            try {
                Integer.parseInt(partMaxField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Max");
            }

            if (Integer.parseInt(partMinField.getText()) > Integer.parseInt(partMaxField.getText())) {
                checkFormat = false;
                alertFunction("Min cannot be greater than Max");
            }
            if (Integer.parseInt(partInStockField.getText()) < Integer.parseInt(partMinField.getText()) 
                    || Integer.parseInt(partInStockField.getText()) > Integer.parseInt(partMaxField.getText())) {
                checkFormat = false;
                alertFunction("Inv cannot be less than min or greater than max");
            }
            if (btnRadioOutSrcd.isSelected()){
                if (checkFormat == true) {   
                    part = new OutSourced(partNameField.getText(), Double.parseDouble(partPriceField.getText()), 
                    Integer.parseInt(partInStockField.getText()), Integer.parseInt(partMinField.getText()), 
                    Integer.parseInt(partMaxField.getText()), companyMachine.getText());
                    Inventory.getInvPartsData().add(part);
                    setMainApp(mainApp);
                    secondaryStage.close();
                }
            }
            if (btnRadioInHse.isSelected()) {
                try {
                    Integer.parseInt(companyMachine.getText());
                }
                catch( NumberFormatException e ) {
                    checkFormat = false;
                    alertFunction("A whole number must be entered for Machine ID");
                }   
                if (checkFormat == true) {
                part = new InHouse(partNameField.getText(), Double.parseDouble(partPriceField.getText()), 
                    Integer.parseInt(partInStockField.getText()), Integer.parseInt(partMinField.getText()), 
                    Integer.parseInt(partMaxField.getText()), Integer.parseInt(companyMachine.getText()));
                    Inventory.getInvPartsData().add(part);
                    setMainApp(mainApp);
                    secondaryStage.close();
                }                    
            }
        }
    }
    
    
    /**
     * handles alert messages
     * @param message message string
     */
    private void alertFunction(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alert");
        alert.setHeaderText("Number Format Incorrect");
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    /**
     * sets machine or company label
     * @param compMachine machine or company label
     */
    private void setCompanyMachineLabel(String compMachine){
        CompanyMachineLabel.setText(compMachine);
    }
    
    /**
     * sets initial label to disappear when company or machine radio button is chosen
     * @param choice 
     */
    private void setChooseLabel(String choice)
    {
        chooseLabel.setText(choice);
    }
    
    public void setMainApp(InventoryMain mainApp) {
        this.mainApp = mainApp;
    }
 }

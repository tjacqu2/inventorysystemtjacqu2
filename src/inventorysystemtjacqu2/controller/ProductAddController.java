/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.controller;

import inventorysystemtjacqu2.InventoryMain;
import inventorysystemtjacqu2.model.Inventory;
import inventorysystemtjacqu2.model.Part;
import inventorysystemtjacqu2.model.Product;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class ProductAddController {

    @FXML
    private TextField searchPartField;
    
    @FXML
    private Button btnCancelSearch;

    @FXML
    private Button btnCancelMenu;

    @FXML
    private Button btnAddToProduct;

    @FXML
    private Button btnSaveNew;
    
    @FXML 
    private Button btnPartDelete;

    @FXML
    private Label labelProdAdd;

    @FXML
    private Label labelParts;
    
    @FXML
    private TextField prodNameField;
    
    @FXML
    private TextField prodIDField;
    
    @FXML
    private TextField prodInvField;
    
    @FXML
    private TextField prodPriceField;
    
    @FXML
    private TextField prodMaxField;
    
    @FXML
    private TextField prodMinField;
    
    @FXML private TableView<Part> PartsTable;
    
    @FXML private TableView<Part> addedPartsTable;
    
    @FXML private TableColumn<Part, Integer> PartTableColumnID;
    
    @FXML private TableColumn<Part, String> PartTableColumnName;
    
    @FXML private TableColumn<Part, Integer> PartTableColumnStock;
    
    @FXML private TableColumn<Part, Double> PartTableColumnPrice;
    
    @FXML private TableColumn<Part, Integer> addedPartTableColumnID;
    
    @FXML private TableColumn<Part, String> addedPartTableColumnName;
    
    @FXML private TableColumn<Part, Integer> addedPartTableColumnStock;
    
    @FXML private TableColumn<Part, Double> addedPartTableColumnPrice;
    
    private InventoryMain mainApp;
    private Stage secondaryStage;
    private Product tempProduct;
    private static double minProdPrice = 0;
    
    @FXML 
    private void initialize() {
        PartTableColumnID.setCellValueFactory(cellData -> cellData.getValue().partIDProperty().asObject());
        PartTableColumnName.setCellValueFactory(cellData -> cellData.getValue().partNameProperty());
        PartTableColumnStock.setCellValueFactory(cellData -> cellData.getValue().partInStockProperty().asObject());
        PartTableColumnPrice.setCellValueFactory(cellData -> cellData.getValue().partPriceProperty().asObject());
        
        addedPartTableColumnID.setCellValueFactory(cellData -> cellData.getValue().partIDProperty().asObject());
        addedPartTableColumnName.setCellValueFactory(cellData -> cellData.getValue().partNameProperty());
        addedPartTableColumnStock.setCellValueFactory(cellData -> cellData.getValue().partInStockProperty().asObject());
        addedPartTableColumnPrice.setCellValueFactory(cellData -> cellData.getValue().partPriceProperty().asObject());
        searchPartField.textProperty().addListener((observable, oldValue, newValue) -> handlePartSearch());
        prodNameField.textProperty().addListener((observable, oldValue, newValue) -> handleNameInput(newValue));
        tempProduct = new Product();//temp group to hold data
    }
    
    /**
     * shows alert if name input starts with a number 
     * @param s name string
     */
    private void handleNameInput(String s) {
        try {
            Integer.parseInt(prodNameField.getText());

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Product name cannot begin with a number");
            alert.setContentText("Begin Product name with a letter or symbol");
            alert.showAndWait();
            prodNameField.setText("");
        } catch(NumberFormatException e) { 
            //do nothing, string is ok
        }
    }

    /**
     * checks if added product name matches an existing product name
     * @return true if matched name found
     */
    private boolean handleProductNameMatch() {
        String searchText = prodNameField.getText();
        FilteredList<Product> searchProdResults = searchProducts(searchText);
        
        return !searchProdResults.isEmpty();
    
    }
    
    /**
     * searches product list
     * @param s name string
     * @return product name
     */
    private FilteredList<Product> searchProducts(String s){
        try {
           Integer.parseInt(s);
           return Inventory.getInvProductData().filtered(p -> Integer.toString(p.getProdID()).equals(s));
        } catch(NumberFormatException e) {
           return Inventory.getInvProductData().filtered(p -> p.getProdName().toLowerCase().equals(s.toLowerCase())); 
        }
    }
    
    /**
     * handles part search when typing in search text field
     */
    @FXML
    private void handlePartSearch() {
        String searchText = searchPartField.getText();
        FilteredList<Part> searchPartResults = searchParts(searchText);
        SortedList<Part> sortedData = new SortedList<>(searchPartResults);
        sortedData.comparatorProperty().bind(PartsTable.comparatorProperty());
        PartsTable.setItems(sortedData);
        if (searchPartResults.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Part Not Found");
            alert.setContentText("No part was found. Re-try search");
            alert.showAndWait();
        }
    }
    
    /**
     * searches part by part ID or name
     * @param s search string or int
     * @return part match
     */
    private FilteredList<Part> searchParts(String s){
        try {
           Integer.parseInt(s);
           return Inventory.getInvPartsData().filtered(p -> Integer.toString(p.getPartID()).contains(s));
        } catch(NumberFormatException e) {
           return Inventory.getInvPartsData().filtered(p -> p.getPartName().toLowerCase().contains(s.toLowerCase())); 
        }
    }
    
    /**
     * clears search text field 
     */
    @FXML
    private void handleBtnCancelSearch() {
        searchPartField.setText("");
    }
    
    /**
     * handles quit of product addition
     */
    @FXML
    void handleBtnCancelMenu() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Quit");
        alert.setHeaderText("Quit Product Add Action?");
        alert.setContentText("Would you like to quit addition of this product?");
        alert.showAndWait()
            .filter(response -> response == ButtonType.OK)
            .ifPresent(response -> {
                --Product.productIDCounter;
                minProdPrice = 0;
                secondaryStage.close();
            });
    }
    
    /**
     * adds part to product
     */
    @FXML
    void handleBtnAddToProduct() {
    	Part chosenPart = PartsTable.getSelectionModel().getSelectedItem();
        if(chosenPart != null) {
            addedPartsTable.setItems(tempProduct.getAssociatedParts());
            minProdPrice += chosenPart.getPartPrice();
            tempProduct.addAssociatedPart(chosenPart);
            
        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Part is Selected");
            alert.setContentText("A part to add must be selected from the top table");
            alert.showAndWait();
        }
    }

    /**
     * deletes part from product
     */
    @FXML
    void handleBtnDeletePart() {
    	Part chosenPart	= addedPartsTable.getSelectionModel().getSelectedItem();
        
        if(chosenPart != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Part Delete");
            alert.setHeaderText("Part will be deleted from current product creation only, NOT the database");
            alert.setContentText("Would you like to delete this part?");
            alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> tempProduct.getAssociatedParts().remove(chosenPart));
            minProdPrice -= chosenPart.getPartPrice();
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No Selection");
            alert.setHeaderText("No Part is Selected");
            alert.setContentText("A part in bottom table must be selected for deletion");
            alert.showAndWait();
        }
    }
    
    /**
     * saves added product to inventory list
     */
    @FXML
    void handleBtnSaveNewProd() {
        if(addedPartsTable.getItems().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Product");
            alert.setHeaderText("No Parts Added to Product");
            alert.setContentText("At least one part must be added to bottom table");
            alert.showAndWait(); 
        }
        else if (prodNameField.getText().isEmpty() 
            || prodPriceField.getText().isEmpty() || prodInvField.getText().isEmpty()
            || prodMinField.getText().isEmpty() || prodMaxField.getText().isEmpty()) 
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Empty Fields");
            alert.setHeaderText("All Fields Not Populated");
            alert.setContentText("All fields must be filled out");
            alert.showAndWait();
        }
        else if (handleProductNameMatch()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("Duplicate Product Name");
            alert.setContentText("Rename Product");
            alert.showAndWait();
        }
        else{
        boolean checkFormat = true;
            try {
                Integer.parseInt(prodInvField.getText());
            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Inv");
            }
            try {
                Double.parseDouble(prodPriceField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A number must be entered for Price/Cost");
            }
            try {
                Integer.parseInt(prodMinField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Min");
            }
            try {
                Integer.parseInt(prodMaxField.getText());

            }
            catch( NumberFormatException e ) {
                checkFormat = false;
                alertFunction("A whole number must be entered for Max");
            }

            if (Integer.parseInt(prodMinField.getText()) > Integer.parseInt(prodMaxField.getText())) {
                checkFormat = false;
                alertFunction("Min cannot be greater than Max");
            }
            if (Integer.parseInt(prodInvField.getText()) < Integer.parseInt(prodMinField.getText()) 
                    || Integer.parseInt(prodInvField.getText()) > Integer.parseInt(prodMaxField.getText())) {
                checkFormat = false;
                alertFunction("Inv cannot be less than min or greater than max");
            }
            if (Double.parseDouble(prodPriceField.getText()) < minProdPrice) {
                checkFormat = false;
                alertFunction("Product price cannot be less than $" + minProdPrice);
            }
            if (checkFormat == true) {
                //tempProduct = new Product();
                tempProduct.setProdName(prodNameField.getText());
                tempProduct.setProdInStock(Integer.parseInt(prodInvField.getText()));
                tempProduct.setProdPrice(Double.parseDouble(prodPriceField.getText()));
                tempProduct.setProdMin(Integer.parseInt(prodMinField.getText()));
                tempProduct.setProdMax(Integer.parseInt(prodMaxField.getText()));
                Inventory.InvProductData.add(tempProduct);
                minProdPrice = 0;
                setMainApp(mainApp);
            }
        }
    }
    
    /**
     * handles alert messages
     * @param message message string
     */
    private void alertFunction(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alert");
        alert.setHeaderText("Number Format Incorrect");
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    public void setSecondaryStage(Stage secondaryStage) {
		this.secondaryStage = secondaryStage;
    }
    
    public void setMainApp(InventoryMain mainApp) {
        this.mainApp = mainApp;
        PartsTable.setItems(Inventory.getInvPartsData());
        secondaryStage.close();
    }
    /**
     * Initializes the controller class.
     */
    //@Override
    //public void initialize(URL url, ResourceBundle rb) {
        // TODO
    //}    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tomjacques
 */
public class OutSourced extends Part {
    
    private StringProperty companyMachine;
     
    public OutSourced(){
        
    }
    //constructor used to overwrite existing part on modify
    public OutSourced(int partID, String name, double price, int inStock, int min, int max, String companyMachine){
        this.partID = new SimpleIntegerProperty(partID);
        this.name = new SimpleStringProperty(name);
        this.price = new SimpleDoubleProperty(price);
        this.inStock = new SimpleIntegerProperty(inStock);
        this.min = new SimpleIntegerProperty(min);
        this.max = new SimpleIntegerProperty(max);
        this.companyMachine = new SimpleStringProperty(companyMachine);
        this.outSourced = true;
    }
    
    public OutSourced(String name, double price, int inStock, int min, int max, String companyMachine){
        partIDCounter++;
        this.partID = new SimpleIntegerProperty(partIDCounter);
        this.name = new SimpleStringProperty(name);
        this.price = new SimpleDoubleProperty(price);
        this.inStock = new SimpleIntegerProperty(inStock);
        this.min = new SimpleIntegerProperty(min);
        this.max = new SimpleIntegerProperty(max);
        this.companyMachine = new SimpleStringProperty(companyMachine);
        this.outSourced = true;
    }
    

    /**
     * @return the companyMachine
     */
    public String getCompanyName() {
        return companyMachine.get();
    }

    /**
     * @param companyName the companyMachine to set
     */
    public void setCompanyName(String companyName) {
        this.companyMachine = new SimpleStringProperty(companyName);
    }
    
    public StringProperty companyNameProperty() {
        return companyMachine;
    }
            
    public void PartIDProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

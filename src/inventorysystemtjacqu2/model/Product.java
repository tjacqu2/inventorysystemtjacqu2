/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.model;

import inventorysystemtjacqu2.InventoryMain;
import inventorysystemtjacqu2.controller.PartModController;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class Product {
    //private String temp;
    private StringProperty name;
    private final ObservableList<Part> associatedParts = FXCollections.observableArrayList();
    private IntegerProperty productID;
    private DoubleProperty price;
    private IntegerProperty inStock;
    private IntegerProperty min;
    private IntegerProperty max;
    public static int productIDCounter = 0;
    
    public Product() {
       productIDCounter++; 
       this.productID = new SimpleIntegerProperty(productIDCounter);
    }
    //constructor used only to create a product with no parts to prove it can be deleted
    public Product(String name, int inStock, double price, int min, int max) {
        productIDCounter++;        
        this.name = new SimpleStringProperty(name);
        this.productID = new SimpleIntegerProperty(productIDCounter);
        this.inStock = new SimpleIntegerProperty(inStock);
        this.price = new SimpleDoubleProperty(price);
        this.min = new SimpleIntegerProperty(min);
        this.max = new SimpleIntegerProperty(max);
    }
    //constructor used to overwrite existing product on modify
    public Product(String name, int productID, int inStock, double price, int min, int max, Part associatedParts) {//,         
        this.name = new SimpleStringProperty(name);
        this.productID = new SimpleIntegerProperty(productID);
        this.inStock = new SimpleIntegerProperty(inStock);
        this.price = new SimpleDoubleProperty(price);
        this.min = new SimpleIntegerProperty(min);
        this.max = new SimpleIntegerProperty(max);
        this.associatedParts.add(associatedParts);
    }
    
    public Product(String name, int inStock, double price, int min, int max, Part associatedParts) {//, 
        productIDCounter++;        
        this.name = new SimpleStringProperty(name);
        this.productID = new SimpleIntegerProperty(productIDCounter);
        this.inStock = new SimpleIntegerProperty(inStock);
        this.price = new SimpleDoubleProperty(price);
        this.min = new SimpleIntegerProperty(min);
        this.max = new SimpleIntegerProperty(max);
        this.associatedParts.add(associatedParts);
    }
    private InventoryMain mainApp; 
    /**
     * @return the productID
     */
    public int getProdID() {
        return productID.get();
    }

    /**
     * @param productID the productID to set
     */
    public void setProdID(int productID) {
        this.productID = new SimpleIntegerProperty(productID);
    }
    
    public IntegerProperty prodIDProperty() {
    		return productID;
    }

    /**
     * @return the name
     */
    public String getProdName() {
        return name.get();
    }

    /**
     * @param name the name to set
     */
    public void setProdName(String name) {
        this.name = new SimpleStringProperty(name);
    }
    
    public StringProperty prodNameProperty() {
    		return name;
    }

    /**
     * @return the price
     */
    public double getProdPrice() {
        return price.get();
    }

    /**
     * @param price the price to set
     */
    public void setProdPrice(double price) {
        this.price = new SimpleDoubleProperty(price);
    }
    
    public DoubleProperty prodPriceProperty() {
    		return price;
    }

    /**
     * @return the inStock
     */
    public int getProdInStock() {
       return inStock.get();
    }

    /**
     * @param inStock the inStock to set
     */
    public void setProdInStock(int inStock) {
        this.inStock = new SimpleIntegerProperty(inStock);
    }
    
    public IntegerProperty prodInStockProperty() {
    		return inStock;
    }

    /**
     * @return the min
     */
    public int getProdMin() {
        return min.get();
    }

    /**
     * @param min the min to set
     */
    public void setProdMin(int min) {
        this.min = new SimpleIntegerProperty(min);
    }

    public IntegerProperty prodMinProperty() {
    		return min;
    }
    /**
     * @return the max
     */
    public int getProdMax() {
        return max.get();
    }

    /**
     * @param max the max to set
     */
    public void setProdMax(int max) {
        this.max = new SimpleIntegerProperty(max);
    }
    
    public IntegerProperty prodMaxProperty() {
    		return max;
    }
    
    
    public ObservableList <Part> getAssociatedParts() {
    		return associatedParts;
    }
    
    public void addAssociatedPart(Part part){
        associatedParts.add(part);
    }
    
    public boolean removeAssociatedPart(int part){
        return true;
    }
    
    public int lookupAssociatedPart(int part){
        return part;
    }
}

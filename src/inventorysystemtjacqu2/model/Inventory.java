/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class Inventory {
    public static ObservableList<Part> InvPartsData = FXCollections.observableArrayList();
    public static ObservableList<Product> InvProductData = FXCollections.observableArrayList();
    
    public static ObservableList<Part> getInvPartsData() {
        return InvPartsData;
    }
    
    public static ObservableList<Product> getInvProductData() {
    		return InvProductData;
    }
    
    /* Methods below are implemented in FXML controller modules
    private void addProduct(int product){
        
    }
    
    private boolean removeProduct(int product ){
        return true;
    }
    
    private int lookupProduct(int product){
        return product;
    }
    
    private void updateProduct(int product){
        
    }
    
    private void addPart(int part){
        
    }
    
    private boolean deletePart(int part){
        return true;
    }
    
    private int lookupPart(int part){
        return part;
    }
    
    private void updatePart(int part){
        
    }*/
}

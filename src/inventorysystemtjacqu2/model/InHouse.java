/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;


/**
 *
 * @author tomjacques
 */
public class InHouse extends Part{
    
	private IntegerProperty companyMachine;
       
   public InHouse() {
       
   }
   //constructor used to overwrite existing part on modify
   public InHouse (int partID, String name, double price, int inStock, int min, int max, int companyMachine) {
           
	   this.partID = new SimpleIntegerProperty(partID);
	   this.name = new SimpleStringProperty(name);
	   this.price = new SimpleDoubleProperty (price);
	   this.inStock = new SimpleIntegerProperty (inStock);
	   this.min = new SimpleIntegerProperty (min);
	   this.max = new SimpleIntegerProperty (max);
           this.companyMachine = new SimpleIntegerProperty(companyMachine);
           this.outSourced = false;
   }
    
   public InHouse (String name, double price, int inStock, int min, int max, int companyMachine) {
           partIDCounter++;
	   this.partID = new SimpleIntegerProperty(partIDCounter);
	   this.name = new SimpleStringProperty(name);
	   this.price = new SimpleDoubleProperty (price);
	   this.inStock = new SimpleIntegerProperty (inStock);
	   this.min = new SimpleIntegerProperty (min);
	   this.max = new SimpleIntegerProperty (max);
           this.companyMachine = new SimpleIntegerProperty(companyMachine);
           this.outSourced = false;
   }

    /**
     * @return the companyMachine
     */
    public int getMachineID() {
        return companyMachine.get();
    }

    /**
     * @param machineID the companyMachine to set
     */
    public void setMachineID(int machineID) {
        this.companyMachine = new SimpleIntegerProperty(machineID);
    }
    
    public IntegerProperty machineIDProperty() {
        return companyMachine;
    }
    
    public void PartIDProperty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

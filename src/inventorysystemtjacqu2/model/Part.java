/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventorysystemtjacqu2.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tomjacques
 */
public abstract class Part {
    
    protected IntegerProperty partID;
    protected StringProperty name;
    protected DoubleProperty price;
    protected IntegerProperty inStock;
    protected IntegerProperty min;
    protected IntegerProperty max;
    protected boolean outSourced;
    //protected static IntegerProperty IDCounter;
    public static int partIDCounter = 0;
    
    public Part(){
    		
    }    
    /**
     * @return the name
     */
    public String getPartName() {
        return name.get();
    }

    /**
     * @param name the name to set
     */
    public void setPartName(String name) {
        this.name = new SimpleStringProperty(name);
    }
    

    public StringProperty partNameProperty(){
        return name;
    }
    
    /**
     * @return the price
     */
    public double getPartPrice() {
        return price.get();
    }

    /**
     * @param price the price to set
     */
    public void setPartPrice(double price) {
        this.price = new SimpleDoubleProperty(price);
    }
    
    public DoubleProperty partPriceProperty() {
        return price;
    }
    
    /**
     * @return the inStock
     */
    public int getPartInStock() {
        return inStock.get();
    }

    /**
     * @param inStock the inStock to set
     */
    public void setPartInStock(int inStock) {
        this.inStock = new SimpleIntegerProperty(inStock);
    }
    
    public IntegerProperty partInStockProperty() {
        return inStock;
    }
    
    /**
     * @return the min
     */
    public int getPartMin() {
        return min.get();
    }

    /**
     * @param min the min to set
     */
    public void setPartMin(int min) {
        this.min = new SimpleIntegerProperty(min);
    }
    
    public IntegerProperty partMinProperty() {
        return min;
    }
    
    /**
     * @return the max
     */
    public int getPartMax() {
        return max.get();
    }

    /**
     * @param max the max to set
     */
    public void setPartMax(int max) {
        this.max = new SimpleIntegerProperty(max);
    }
    
    public IntegerProperty partMaxProperty() {
        return max;
    }

    /**
     * @return the partID
     */
    public int getPartID() {
        return partID.get();
    }

    /**
     * @param partID the partID to set
     */
    public void setPartID(int partID) {
        this.partID = new SimpleIntegerProperty(partID);
    }
    
    public IntegerProperty partIDProperty() {
        return partID;
    }
    /**
     * checks if part is inHouse or OutSourced
     * @return true or false depending on part constructor
     */
    public boolean isGetCompany() {
        return outSourced;
    }
}
